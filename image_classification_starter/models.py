import tensorflow as tf
import sys

FLAGS = tf.app.flags.FLAGS

def get_model():
    if FLAGS.model == '':
        print('Please Specify a model for training.')
        sys.exit()
    elif FLAGS.model == 'mnist':
        x = tf.placeholder(tf.float32, [None, FLAGS.dimensions_x, FLAGS.dimensions_y, FLAGS.channels], name='x')
        y_ = tf.placeholder(tf.float32, [None, FLAGS.classes])
        train = tf.placeholder(tf.bool, name='train')
        conv1 = tf.layers.conv2d(inputs=x, filters=32, kernel_size=[5, 5], padding="same", activation=tf.nn.relu)
        pool1 = tf.layers.max_pooling2d(inputs=conv1, pool_size=[2, 2], strides=2)
        conv2 = tf.layers.conv2d(inputs=pool1, filters=64, kernel_size=[5, 5], padding="same", activation=tf.nn.relu)
        pool2 = tf.layers.max_pooling2d(inputs=conv2, pool_size=[2, 2], strides=2)
        flat = tf.reshape(pool2, [-1, int(FLAGS.dimensions_x / (2.0*2.0)) * int(FLAGS.dimensions_y / (2.0*2.0)) * 64])
        dense1 = tf.layers.dense(inputs=flat, units=1024, activation=tf.nn.relu)
        dropout1 = tf.layers.dropout(inputs=dense1, rate=0.4, training=train)
        y = tf.layers.dense(inputs=dropout1, units=FLAGS.classes, name='predict')
        loss = tf.losses.softmax_cross_entropy(onehot_labels=y_, logits=y)
        train_step = tf.contrib.layers.optimize_loss(loss=loss, global_step=tf.train.get_global_step(),
                                                     learning_rate=0.0001, optimizer="Adam")
        correct_prediction = tf.equal(tf.argmax(y, 1), tf.argmax(y_, 1))
        accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))
        return train_step, accuracy, x, y_, train
    else:
        print('Please Specify a existing model for training.')
        sys.exit()
