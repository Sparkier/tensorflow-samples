import tensorflow as tf

FLAGS = tf.app.flags.FLAGS

def parse_function(proto):
    features={
        'image/height': tf.FixedLenFeature([], tf.int64),
        'image/width': tf.FixedLenFeature([], tf.int64),
        'image/colorspace': tf.FixedLenFeature([], dtype=tf.string, default_value=''),
        'image/channels': tf.FixedLenFeature([], tf.int64),
        'image/class/label': tf.FixedLenFeature([], tf.int64),
        'image/class/text': tf.FixedLenFeature([], dtype=tf.string, default_value=''),
        'image/format': tf.FixedLenFeature([], dtype=tf.string, default_value=''),
        'image/filename': tf.FixedLenFeature([], dtype=tf.string, default_value=''),
        'image/encoded': tf.FixedLenFeature([], dtype=tf.string, default_value='')
    }
    parsed_features = tf.parse_single_example(proto, features)

    # Convert from a scalar string tensor (whose single string has
    # length mnist.IMAGE_PIXELS) to a uint8 tensor with shape
    # [mnist.IMAGE_PIXELS].
    image_buffer = parsed_features['image/encoded']
    image_label = tf.cast(parsed_features['image/class/label'], tf.int32)

    # Decode the jpeg
    with tf.name_scope('decode_jpeg', [image_buffer], None):
        # decode
        image = tf.image.decode_jpeg(image_buffer, channels=3)

        # and convert to single precision data type
        image = tf.image.convert_image_dtype(image, dtype=tf.float32)
        if(FLAGS.channels == 1):
            image = tf.image.rgb_to_grayscale(image)

    image_shape = tf.stack([FLAGS.dimensions_x, FLAGS.dimensions_y, FLAGS.channels])

    image = tf.reshape(image, image_shape)
    image_label = tf.one_hot(image_label - 1, FLAGS.classes)

    return image, image_label

def get_dataset(train_files, val_files):
    # Training Dataset
    train_dataset = tf.data.TFRecordDataset(train_files)
    train_dataset = train_dataset.map(parse_function)  # Parse the record into tensors.
    train_dataset = train_dataset.shuffle(buffer_size=10000)
    train_dataset = train_dataset.batch(40)
    # Validation Dataset
    validation_dataset = tf.data.TFRecordDataset(val_files)
    validation_dataset = validation_dataset.map(parse_function)
    validation_dataset = validation_dataset.batch(40)

    # A feedable iterator is defined by a handle placeholder and its structure. We
    # could use the `output_types` and `output_shapes` properties of either
    # `training_dataset` or `validation_dataset` here, because they have
    # identical structure.
    handle = tf.placeholder(tf.string, shape=[])
    iterator = tf.data.Iterator.from_string_handle(handle, train_dataset.output_types,
                                                           train_dataset.output_shapes)
    next_element = iterator.get_next()

    # Generate the Iterators
    training_iterator = train_dataset.make_initializable_iterator()
    validation_iterator = validation_dataset.make_initializable_iterator()

    return training_iterator, validation_iterator, next_element, handle
