# Image Classification Starter
This aims at providing a starting point for image classification projects. To start an image classification project, simply copy this folder and follow the instructions within this document.

## Usage
The following describes how to use this template. You will probably not need everything provided in here, so feel free to skip/modify/replace any step within this description.

### Data Transformation
To be able to efficiently work with large image sets, I chose to convert them to Tensorflows TFRecords data format. This makes everything more efficient since the whole dataset is saved into one/some binary files rather than accessing each image individually.

To make this conversion, I included the python file `images_to_tfrecords.py`. I copied this from google without any modifications. The file itself has instructions on how to use it.

This is a preprocessing step and thus executed only once independently of training.

### Training

To train a network, I use the file `training.py`. Here the Tensorflow session gets initialized and training started.

However, you of course also need to be able to read TFRecords files back so it is usable in training a neural network with Tensorflow. To do this, I wrote the file `read_tfrecords.py`. This is used by `training.py` as a helper function.

To also provide the models, `model.py` is another helper function defining the model architectures. If you want to build your own model, you should do it here and return the appropriate variables.

## Example
1. Download the MNIST training set from the data folder and extract it (here, extracted in downloads, for other paths, modify the following steps).

2. Convert the images to tfrecords.
`python images_to_tfrecords.py --train_directory='~/Downloads/MNIST/train' --validation_directory='~/Downloads/MNIST/test' --output_directory='~/Downloads/MNIST' --labels_file='~/Downloads/MNIST/labels.txt'`

3. Initiate the training.
`python training.py --data_dir='~/Downloads' --dataset='MNIST' --dimensions_x=28  -dimensions_y=28---model='mnist' --classes=10`

Feel free to experiment with and modify the network or training process. This model gets to ~99.3% accuracy, however, there is still room for improvement.