import tensorflow as tf
import sys
import os

import read_tfrecords as rt # Helper to read TFRecords files.
import models as mod # Helper providing model architectures.

# Variables that have to be passed for the training run to be executed correctly.
tf.app.flags.DEFINE_string('data_dir', '', 'Directory where the dataset is saved.')
tf.app.flags.DEFINE_string('dataset', '', 'Name of the dataset folder.')
tf.app.flags.DEFINE_integer('dimensions_x', 0, 'Dimensions in X of images.')
tf.app.flags.DEFINE_integer('dimensions_y', 0, 'Dimensions in Y of images.')
tf.app.flags.DEFINE_string('model', '', 'Training model.')
tf.app.flags.DEFINE_integer('channels', 1, 'Number of color channels for the image.')
tf.app.flags.DEFINE_integer('classes', 0, 'Number of classes to be trained on.')
tf.app.flags.DEFINE_integer('batch_size', 64, 'Images per batch in training.')
tf.app.flags.DEFINE_integer('epochs', 20, 'Number of epochs to train.')
FLAGS = tf.app.flags.FLAGS

# Initializes all needed functions and starts the training process.
def train(classification_path):
    # Get the dataset iterators.
    (training_iterator, 
        validation_iterator, 
        next_element, 
        handle) = rt.get_dataset([classification_path +
            '/train-00000-of-00001'], # Modify, if your file is named differently!
            [classification_path +
            '/validation-00000-of-00001']) # Modify, if your file is named differently!
    # Get the training model.
    train_step, accuracy, x, y_, train = mod.get_model()

    # For saving the current Model
    saver = tf.train.Saver()

    # Sometimes needed to not exceed GPU memory.
    gpu_options = tf.GPUOptions(
                per_process_gpu_memory_fraction=0.7,
                allow_growth=True)
    # Initialize Session.
    sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
    # Initialize Variables.
    sess.run(tf.global_variables_initializer())

    # The `Iterator.string_handle()` method returns a tensor that can be evaluated
    # and used to feed the `handle` placeholder.
    training_handle = sess.run(training_iterator.string_handle())
    validation_handle = sess.run(validation_iterator.string_handle())

    # Counting the iterations of the Training
    train_iterations = 0

    # Compute for x epochs.
    for i in range(FLAGS.epochs):
        def _helper():
            # Initialize Training Data
            sess.run(training_iterator.initializer)
            while True:
                try:
                    # Run Training as long as more Data available
                    img, lbl = sess.run(next_element, feed_dict={handle: training_handle})
                    sess.run(train_step, feed_dict={x: img, y_: lbl, train: True})
                except tf.errors.OutOfRangeError:
                    # No more Training Data, Evaluate and save the Model
                    avg_acc = 0.0
                    sess.run(validation_iterator.initializer)
                    j = 0
                    while True:
                        try:
                            # Evaluate for all the Validation Data
                            img, lbl = sess.run(next_element, feed_dict={handle: validation_handle})
                            acc = sess.run(accuracy, feed_dict={x: img, y_: lbl, train: False})
                            avg_acc = avg_acc + acc
                            j = j + 1
                        except tf.errors.OutOfRangeError:
                            # No more Validation Data, show Progress and save the model
                            avg_acc = avg_acc / j
                            print('Validation accuracy after epoch ' + str(i+1) + ': ' + str(avg_acc))
                            # Save the model after every epoch
                            saver.save(sess, classification_path + '/saved_models/saved_model',
                                        global_step=tf.train.get_global_step())
                            return
        _helper()

# Assert that variables were properly set.
if (FLAGS.dataset == '' or 
        FLAGS.dimensions_x == 0 or 
        FLAGS.dimensions_y == 0 or 
        FLAGS.model == '' or 
        FLAGS.classes == 0):
    print('You missed to specify some needed variables.')
    sys.exit()

# Start the training.
train(os.path.join(FLAGS.data_dir + FLAGS.dataset))
