# Tensorflow Samples

*This is outdated. It has not been updated for new Tensorflow versions.*

It can be a tedious task to build up every step needed for a machine learning project from scratch. To help you get some inspiration on how to start a new project, I decided to provide the code I used for my machine learning applications within this repository.

This is intended as inspiration and foundation to build on, so feel free to look into what I did with Tensorflow.

## Content
This repository is sructured in different subfolders. The following will briefly describe their content.

- **image_classification_starter**: A starting point for image classification projects. Can be customized to your needs. 

- **data**: A location for example data. 
